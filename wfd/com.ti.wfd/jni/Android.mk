LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_C_INCLUDES := . \
        $(JNI_H_INCLUDE) \
        $(TOP)/frameworks/native/include \
        $(TOP)/frameworks/native/include/media/openmax \
        $(TOP)/frameworks/base/include \
        $(TOP)/frameworks/av/include/media/stagefright/foundation \
        $(TOP)/frameworks/av/media/libstagefright/wifi-display

LOCAL_SRC_FILES  := wfdsink_jni.cpp \

LOCAL_SHARED_LIBRARIES := libcutils \
                          libutils \
                          libnativehelper \
                          libgui \
                          libstagefright \
                          libstagefright_foundation \
                          libstagefright_wfd

LOCAL_MODULE     := libwfdsink_jni

include $(BUILD_SHARED_LIBRARY)
