/*
 * Copyright (C) Texas Instruments - http://www.ti.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <jni.h>
#include <JNIHelp.h>

#include <utils/StrongPointer.h>
#include <utils/Log.h>

#include <gui/ISurfaceTexture.h>
#include <gui/Surface.h>

#include <pthread.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <unistd.h>

#include <sink/WifiDisplaySink.h>

#define LOG_TAG "wfdsink_jni"

using namespace android;

/* TODO:
 * Looper and WifiDisplaySink objects declared static to allow JNI functions access
 * to native part of WFD sink. This approach means that only one WFD sink can exist
 * in a system at a time. For current implementation this seems sufficient because
 * only one WFD sink required per device.
 * In future this part must be reworked to store WifiDisplaySink pointer in java
 * RtspSink to allow multiple sink objects creation and avoid static variables
 * usage in libraries.
 **/

static sp<ALooper> looper = NULL;
static sp<WifiDisplaySink> sink = NULL;

class NativeRtspStateListener : public RtspStateListener {
public:
    NativeRtspStateListener(JavaVM *jvm, jobject listener);
    ~NativeRtspStateListener();
    void attachListener();
    void onStateChanged(int State);
private:
    JavaVM *mJvm;
    jobject mListener;
    jmethodID mOnStateChangedId;
    bool mIsListenerAttached;
};

NativeRtspStateListener::NativeRtspStateListener(JavaVM *jvm, jobject listener)
    : mJvm(jvm),
      mListener(NULL),
      mOnStateChangedId(NULL),
      mIsListenerAttached(false)
{
    JNIEnv *e;
    if (jvm->GetEnv((void**)&e, JNI_VERSION_1_4) != JNI_OK) {
        ALOGE("Error getting Env var");
        return;
    }
    mListener = e->NewGlobalRef(listener);
    jclass listenerClass = e->FindClass("com/ti/omap/android/wfd/sink/WfdStateListener");
    mOnStateChangedId = e->GetMethodID(listenerClass, "onStateChanged", "(I)V");
}

NativeRtspStateListener::~NativeRtspStateListener() {
    JNIEnv *e;
    if (mJvm->GetEnv((void**)&e, JNI_VERSION_1_4) != JNI_OK) {
        ALOGE("Error getting Env var");
    } else {
        e->DeleteGlobalRef(mListener);
    }
    if (mJvm->DetachCurrentThread() != JNI_OK) {
        ALOGE("Error detaching native thread from java");
    }
}

void NativeRtspStateListener::attachListener() {
    JNIEnv *e;
    if (mJvm->AttachCurrentThread(&e, NULL) != JNI_OK) {
        ALOGE("Error attaching native thread to java");
        mIsListenerAttached = false;
        return;
    }
    mIsListenerAttached = true;
}

void NativeRtspStateListener::onStateChanged(int state) {
    JNIEnv *e;

    if (!mIsListenerAttached) {
        attachListener();
        if (!mIsListenerAttached) {
            ALOGE("Error attaching native thread. Can't invoke listener");
            return;
        }
    }

    if (mJvm->GetEnv((void**)&e, JNI_VERSION_1_4) != JNI_OK) {
        ALOGE("Error getting Env var");
        return;
    }
    e->CallVoidMethod(mListener, mOnStateChangedId, state);
}


static sp<ISurfaceTexture> getSurface(JNIEnv *e, jobject thisObj,
        jobject jSurface) {
    sp<ISurfaceTexture> new_st = NULL;

    if (!jSurface) {
        ALOGE("NULL Surface got as argument");
        return new_st;
    }

    jclass surfaceClass = e->FindClass("android/view/Surface");

    if (surfaceClass == NULL) {
        ALOGE("Error getting Surface class");
        return new_st;
    }

    jfieldID surfaceId = e->GetFieldID(surfaceClass, "mNativeSurface", "I");

    if (surfaceId == NULL) {
        ALOGE("Error getting Surface field");
        return new_st;
    }

    jint surfaceValue = e->GetIntField(jSurface, surfaceId);

    sp<Surface> surface((Surface*) surfaceValue);
    if (surface != NULL) {
        new_st = surface->getSurfaceTexture();
        new_st->incStrong(thisObj);
    } else {
        jniThrowException(e, "java/lang/IllegalArgumentException",
                "The surface has been released");
        return new_st;
    }

    return new_st;
}

jboolean Java_com_ti_omap_android_wfd_sink_WfdSink_connect(JNIEnv *e,
        jobject thisObj, jobject jSurface, jstring url) {
    char *utfUrl = (char*) e->GetStringUTFChars(url, JNI_FALSE);

    int port;
    char * ip = new char[strlen(utfUrl) + 1];
    strcpy(ip, utfUrl);
    char *colonPos = strchr(ip, ':');
    if (colonPos != NULL) {
        char *end;
        unsigned long x = strtoul(colonPos + 1, &end, 10);

        if (end == colonPos + 1 || *end != '\0' || x >= 65536) {
            return JNI_FALSE;
        }

        *colonPos = '\0';
        port = x;
    } else {
        port = 7236;
    }
    ALOGI("Sink. Connecting ... URL: %s:%d", ip, port);
    if (sink == NULL) {
        sp<ANetworkSession> netSession = new ANetworkSession();
        netSession->start();

        looper = new ALooper();

        sink = new WifiDisplaySink(netSession,
                getSurface(e, thisObj, jSurface));
        looper->setName("wfd_sink_looper");
        looper->registerHandler(sink);
        looper->start();

        sink->start(ip, port);
    } else {
        sink->start(ip, port);
    }
    delete[] ip;
    e->ReleaseStringUTFChars(url, utfUrl);

    return JNI_TRUE;
}

jboolean Java_com_ti_omap_android_wfd_sink_WfdSink_disconnect(JNIEnv *e,
        jobject thisObj) {
    // TODO implement rtsp disconnect
    jboolean result = JNI_TRUE;
    ALOGI("Sink. Disconnecting ...");
    return result;
}

jboolean Java_com_ti_omap_android_wfd_sink_WfdSink_play(JNIEnv *e,
        jobject thisObj) {
    jboolean result = JNI_TRUE;
    ALOGI("Sink. play");
    if (sink != NULL) {
        sink->play();
    }
    return result;
}

jboolean Java_com_ti_omap_android_wfd_sink_WfdSink_pause(JNIEnv *e,
        jobject thisObj) {
    jboolean result = JNI_TRUE;
    ALOGI("Sink. pause");
    if (sink != NULL) {
        sink->pause();
    }
    return result;
}

jboolean Java_com_ti_omap_android_wfd_sink_WfdSink_teardown(JNIEnv *e,
        jobject thisObj) {
    jboolean result = JNI_TRUE;
    ALOGI("Sink. teardown");
    if (sink != NULL) {
        sink->teardown();
    }
    return result;
}

void Java_com_ti_omap_android_wfd_sink_WfdSink_setStateListener(JNIEnv *e,
        jobject thisObj,
        jobject listenerObj) {
    ALOGV("Setting java listener");

    if (sink != NULL) {
        JavaVM *jvm;
        sp<RtspStateListener> rtspStateListener;

        e->GetJavaVM(&jvm);
        rtspStateListener = new NativeRtspStateListener(jvm, listenerObj);
        sink->setRtspStateListener(rtspStateListener);
    }
}

void Java_com_ti_omap_android_wfd_sink_WfdSink_removeStateListener(JNIEnv *e,
        jobject thisObj) {
    ALOGV("Removing java listener");
    if (sink != NULL) {
        sink->removeRtspStateListener();
    }
}

static JNINativeMethod wfdSinkMethods[] = { { "connect",
        "(Landroid/view/Surface;Ljava/lang/String;)Z",
        (void*) Java_com_ti_omap_android_wfd_sink_WfdSink_connect }, {
        "disconnect", "()Z",
        (void*) Java_com_ti_omap_android_wfd_sink_WfdSink_disconnect }, {
        "play", "()Z",
        (void*) Java_com_ti_omap_android_wfd_sink_WfdSink_play }, {
        "pause", "()Z",
        (void*) Java_com_ti_omap_android_wfd_sink_WfdSink_pause }, {
        "teardown", "()Z",
        (void*) Java_com_ti_omap_android_wfd_sink_WfdSink_teardown }, {
        "setStateListener", "(Lcom/ti/omap/android/wfd/sink/WfdStateListener;)V",
        (void*) Java_com_ti_omap_android_wfd_sink_WfdSink_setStateListener }, {
        "removeStateListener", "()V",
        (void*) Java_com_ti_omap_android_wfd_sink_WfdSink_removeStateListener }
        };

extern "C" jint JNI_OnLoad(JavaVM *jvm, void *reserved) {
    JNIEnv *e;

    if (jvm->GetEnv((void**) &e, JNI_VERSION_1_4) != JNI_OK) {
        ALOGE("Error getting Env var");
        return JNI_ERR;
    }

    jniRegisterNativeMethods(e, "com/ti/omap/android/wfd/sink/WfdSink",
            wfdSinkMethods, NELEM(wfdSinkMethods));

    return JNI_VERSION_1_4;
}
