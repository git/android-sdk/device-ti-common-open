LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := com.ti.wfd

LOCAL_MODULE_TAGS := optional

LOCAL_REQUIRED_MODULES := librtspsink_jni

LOCAL_SRC_FILES := $(call all-subdir-java-files)

include $(BUILD_JAVA_LIBRARY)

include $(call all-makefiles-under, $(LOCAL_PATH))
