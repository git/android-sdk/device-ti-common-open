/*
 * Copyright (C) Texas Instruments - http://www.ti.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ti.omap.android.wfd.sink;

import android.os.SystemProperties;
import android.view.Surface;

public class WfdSink {

    private String mIpAddress;
    private int mPort;

    public WfdSink(String ipAddress, int port) throws WfdSinkRoleException {
        String wfdRole = SystemProperties.get("persist.wfd.role", "source");

        if (!wfdRole.equalsIgnoreCase("sink")) {
            throw (new WfdSinkRoleException());
        }

        mIpAddress = ipAddress;
        mPort = port;
    }

    static {
        System.loadLibrary("wfdsink_jni");
    }

    boolean connect(Surface surface) {
        String url = mIpAddress + ":" + mPort;
        return connect(surface, url);
    }

    private native boolean connect(Surface surface, String url);

    public native boolean disconnect();

    public native boolean play();

    public native boolean pause();

    public native boolean teardown();

    public native void setStateListener(WfdStateListener listener);

    public native void removeStateListener();
}
