/*
 * Copyright (C) Texas Instruments - http://www.ti.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ti.omap.android.wfd.sink;

import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceView;

public class WfdSinkSurfaceView extends SurfaceView {
    private Surface mSurface;
    private boolean mPreserveSurface;

    public WfdSinkSurfaceView(Context context) {
        super(context);
        mSurface = getHolder().getSurface();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent ke) {
        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent ke) {
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent me) {
        return true;
    }

    @Override
    public boolean onTrackballEvent(MotionEvent me) {
        return true;
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent me) {
        return true;
    }

    public Surface getSurface() {
        if (mSurface == null) {
            mSurface = getHolder().getSurface();
        }
        return mSurface;
    }

    @Override
    protected void onWindowVisibilityChanged(int visibility) {
        if (mPreserveSurface) {
            super.onWindowVisibilityChanged(VISIBLE);
        } else {
            super.onWindowVisibilityChanged(visibility);
        }
    }

    @Override
    public void setVisibility(int visibility) {
        if (mPreserveSurface) {
            super.setVisibility(VISIBLE);
        } else {
            super.setVisibility(visibility);
        }
    }

}
