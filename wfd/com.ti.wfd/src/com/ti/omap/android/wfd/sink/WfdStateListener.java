package com.ti.omap.android.wfd.sink;

public interface WfdStateListener {
    public void onStateChanged(int newState);
}
