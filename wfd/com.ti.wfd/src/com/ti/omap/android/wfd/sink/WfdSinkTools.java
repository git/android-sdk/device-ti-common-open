/*
 * Copyright (C) Texas Instruments - http://www.ti.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ti.omap.android.wfd.sink;

public class WfdSinkTools {
    public static final int STATE_UNDEFINED                       =  -1;
    public static final int STATE_IDLE                            =  0;
    public static final int STATE_CONNECTING                      =  1;
    public static final int STATE_CONNECTED                       =  2;
    public static final int STATE_PLAYING                         =  3;
    public static final int STATE_PAUSED                          =  4;
    public static final int STATE_DISCONNECTING                   =  5;

    public static final int DEFAULT_RTSP_PORT                     = 7236;

    public static final String START_SINK_ACTION                  = "com.ti.wfd.sink.start";

    public static final String EXTRA_ADDRESS                      = "EXTRA_ADDRESS";
    public static final String EXTRA_PORT                         = "EXTRA_PORT";

    public static final String DHCP_ACTION                        = "com.ti.wfd.dhcp.action";

    public static final String EXTRA_MAC_ADDRESS                  = "MACAddress";
    public static final String EXTRA_ACTION                       = "Action";
    public static final String EXTRA_IP_ADDRESS                   = "IPAddress";

    public static final String LEASES_ACTION_OLD                  = "old";
    public static final String LEASES_ACTION_ADD                  = "add";
}
