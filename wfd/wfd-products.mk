#Artifacts associated with WFD support

PRODUCT_COPY_FILES += \
	device/ti/common-open/wfd/configs/wfd_source_cfg.xml:system/etc/wfd/wfd_source_cfg.xml \
	device/ti/common-open/wfd/configs/wfd_sink_cfg.xml:system/etc/wfd/wfd_sink_cfg.xml \
	device/ti/common-open/wfd/configs/com.ti.wfd.xml:system/etc/permissions/com.ti.wfd.xml

PRODUCT_PACKAGES += \
        libwfdsink_jni \
        com.ti.wfd \
        dhcp_script.sh \
        WfdSinkService \
        WfdSinkPlayer
