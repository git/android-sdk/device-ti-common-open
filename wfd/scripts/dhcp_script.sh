#!/system/bin/sh

#$1 - action
#$2 - MAC address
#$3 - IP address
#$4 - host name

/system/bin/am broadcast -a com.ti.wfd.dhcp.action -e Action "$1" -e MACAddress "$2" -e IPAddress "$3"
